const path = require("path");
const rootPath = __dirname;

module.exports = {
  rootPath,
  db: {
    url: "mongodb://localhost/event1",
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    },
  },
  facebook: {
    appId: "254680806442031",
    appSecret: "e9d42d3bca98712e91c5e2b7a325b675",
  },
};
