import axios from 'axios';
import {apiURL} from "./config";

const axiosCalendar = axios.create({
  baseURL: apiURL
});

export default axiosCalendar;