import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./Components/UI/Layout/Layout";
import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import Events from "./Containers/Events/Events";
import AddNewFriend from "./Containers/AddNewFriend/AddNewFriend";
import AddNewEvent from "./Containers/AddNewEvent/AddNewEvent";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);

  return (
    <Layout>
      <Switch>
        <ProtectedRoute path="/" exact component={Events} isAllowed={user !== null} redirectTo="/login" />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
        <ProtectedRoute
          path="/add-friend"
          component={AddNewFriend}
          isAllowed={user !== null}
          redirectTo="/login"
        />
        <ProtectedRoute
          path="/add-event"
          exact
          component={AddNewEvent}
          isAllowed={user !== null}
          redirectTo="/login"
        />
      </Switch>
    </Layout>
  );
};

export default App;
