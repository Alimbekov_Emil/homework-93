import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
  user: null,
  friends: [],
};

const name = "users";

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerRequest: (state) => {
      state.registerLoading = true;
    },
    registerSuccess: (state, { payload: user }) => {
      state.registerLoading = false;
      state.user = user;
    },
    registerFailure: (state, { payload: error }) => {
      state.registerLoading = false;
      state.registerError = error;
    },
    loginRequest: (state) => {
      state.loginLoading = true;
    },
    loginSuccess: (state, { payload: user }) => {
      state.loginLoading = false;
      state.user = user;
    },
    loginFailure: (state, { payload: error }) => {
      state.loginLoading = false;
      state.loginError = error;
    },
    facebookLoginRequest: (state) => {
      state.loginLoading = true;
    },

    logoutRequest: () => {},
    logoutSuccess: (state) => {
      state.user = null;
    },
    addNewFriendRequest: (state, action) => {
      state.loginLoading = true;
    },
    addNewFriendSuccess: (state, { payload: user }) => {
      state.loginLoading = false;
      state.user = user;
    },
    addNewFriendFailure: (state, action) => {
      state.loginLoading = false;
    },
    deleteFriendRequest: (state, action) => {
      state.loginLoading = true;
    },
    deleteFriendSuccess: (state, { payload: user }) => {
      state.loginLoading = false;
      state.user = user;
    },
    deleteFriendFailure: (state, action) => {
      state.loginLoading = false;
    },
    fetchFriendRequest: (state, action) => {
      state.loginLoading = true;
    },
    fetchFriendSuccess: (state, { payload: friends }) => {
      state.loginLoading = false;
      state.friends = friends;
    },
    fetchFriendFailure: (state, action) => {
      state.loginLoading = false;
    },
  },
});

export default usersSlice;
