import { put, takeEvery } from "redux-saga/effects";
import { NotificationManager } from "react-notifications";
import {
  addNewFriendFailure,
  addNewFriendRequest,
  addNewFriendSuccess,
  deleteFriendRequest,
  deleteFriendSuccess,
  facebookLoginRequest,
  fetchFriendFailure,
  fetchFriendRequest,
  fetchFriendSuccess,
  loginFailure,
  loginRequest,
  loginSuccess,
  logoutRequest,
  logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
} from "../actions/usersActions";
import { historyPush } from "../actions/historyActions";
import axiosCalendar from "../../axiosCalendar";

export function* registerUser({ payload: userData }) {
  try {
    const response = yield axiosCalendar.post("/users", userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush("/"));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({ payload: userData }) {
  try {
    const response = yield axiosCalendar.post("/users/sessions", userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush("/"));
    NotificationManager.success("Login successful");
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({ payload: data }) {
  try {
    const response = yield axiosCalendar.post("/users/facebookLogin", data);
    console.log(response.data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush("/"));
    NotificationManager.success("Login successful");
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* logout() {
  try {
    yield axiosCalendar.delete("/users/sessions");
    yield put(logoutSuccess());
    yield put(historyPush("/"));
    NotificationManager.success("Logged out!");
  } catch (e) {
    NotificationManager.error("Could not logout");
  }
}

export function* addNewFriend({ payload: id }) {
  try {
    const response = yield axiosCalendar.put("/users/addFriends", { friend: id });
    yield put(addNewFriendSuccess(response.data));
    yield fetchFriends();
  } catch (e) {
    yield put(addNewFriendFailure());
  }
}

export function* deleteFriend({ payload: id }) {
  try {
    const response = yield axiosCalendar.put("/users/deleteFriends", { friend: id });
    if (response.data.message) {
      NotificationManager.error(response.data.message);
    } else {
      yield put(deleteFriendSuccess(response.data));
      yield fetchFriends();
    }
  } catch (e) {
    yield put(addNewFriendFailure());
  }
}

export function* fetchFriends() {
  try {
    const response = yield axiosCalendar.get("/users/friends");
    yield put(fetchFriendSuccess(response.data.friends));
  } catch (e) {
    yield put(fetchFriendFailure(e));
  }
}

const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(logoutRequest, logout),
  takeEvery(addNewFriendRequest, addNewFriend),
  takeEvery(deleteFriendRequest, deleteFriend),
  takeEvery(fetchFriendRequest, fetchFriends),
];

export default usersSagas;
