import eventsSlice from "../slices/eventsSlice";

export const {
  fetchEventsRequest,
  fetchEventsSuccess,
  fetchEventsFailure,
  createEventRequest,
  createEventSuccess,
  createEventFailure,
  deleteEventRequest,
  deleteEventSuccess,
  deleteEventFailure,
} = eventsSlice.actions;
