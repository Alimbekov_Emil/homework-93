import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Menu, MenuItem } from "@material-ui/core";
import { logoutRequest } from "../../../../store/actions/usersActions";
import { Link } from "react-router-dom";

const UserMenu = ({ user }) => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button onClick={handleClick} color="inherit">
        Hello, {user.displayName}!
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        <MenuItem>
          <Link to="/add-friend" style={{ textDecoration: "none", color: "black" }}>
            Add New Friends
          </Link>
        </MenuItem>
        <MenuItem>
          <Link to="/add-event" style={{ textDecoration: "none", color: "black" }}>
            Add New Event
          </Link>
        </MenuItem>
        <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;
