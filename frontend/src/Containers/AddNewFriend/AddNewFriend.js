import { Button, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import FormElement from "../../Components/UI/Form/FormElement";
import { useDispatch, useSelector } from "react-redux";
import {
  addNewFriendRequest,
  deleteFriendRequest,
  fetchFriendRequest,
} from "../../store/actions/usersActions";

const AddNewFriend = () => {
  const dispatch = useDispatch();
  const [friend, setFriend] = useState("");

  const friends = useSelector((state) => state.users.friends);

  useEffect(() => {
    dispatch(fetchFriendRequest());
  }, [dispatch]);

  return (
    <Grid container spacing={2} direction="column">
      <Typography variant="h4">Add New Friends</Typography>
      <Grid item>
        <form>
          <FormElement value={friend} onChange={(e) => setFriend(e.target.value)} />
          <Button
            style={{ margin: "10px" }}
            onClick={() => dispatch(addNewFriendRequest(friend))}
            variant="outlined"
          >
            Add Friend
          </Button>
        </form>
      </Grid>
      <Grid item>
        <Typography variant="h6">Your Friends:</Typography>
        {friends !== []
          ? friends.map((friend) => {
              return (
                <Grid item container key={friend[0]._id}>
                  <p>{friend[0].displayName}</p>
                  <Button onClick={() => dispatch(deleteFriendRequest(friend[0]._id))}>X</Button>
                </Grid>
              );
            })
          : null}
      </Grid>
    </Grid>
  );
};

export default AddNewFriend;
