import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteEventRequest, fetchEventsRequest } from "../../store/actions/eventsActions";
import { Button, Card, CardActions, CardContent, CardHeader, Grid } from "@material-ui/core";

const Events = () => {
  const dispatch = useDispatch();
  const events = useSelector((state) => state.events.events);
  const user = useSelector((state) => state.users.user);

  useEffect(() => {
    dispatch(fetchEventsRequest());
  }, [dispatch]);

  const deleteEvent = (id) => {
    dispatch(deleteEventRequest(id));
  };

  return (
    <Grid container spacing={2} direction="column">
      <Grid item container>
        {events.map((event) => (
          <Grid item xs sm md={6} lg={6} key={event._id}>
            <Card style={{ margin: "10px" }}>
              <CardHeader title={event.title} />
              <CardContent>
                Date:{event.date}
                <p>Duration: {event.duration}</p>
                <p>Author: {event.author.displayName}</p>
              </CardContent>
              <CardActions>
                {user._id === event.author._id ? (
                  <Button variant="outlined" onClick={() => deleteEvent(event._id)}>
                    Delete
                  </Button>
                ) : null}
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default Events;
